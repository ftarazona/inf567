class eMBBFlow:
    def __init__(self, rate):
        self.rate = rate

import math

def eMBB_schedule(flows, n_scs, simutime):
    sumrate = sum([flow.rate for flow in flows])
    rates = [flow.rate / sumrate for flow in flows]
    n_res = [math.floor(rate * n_scs * simutime) for rate in rates]
    grid = [[0 for i in range(simutime)] for j in range(n_scs)]
    scheduled = [0] * len(flows)

    current_flow = 0
    current_re = 0

    for n in n_res:
        for i in range(n):
            grid[current_re // simutime][current_re % simutime] = current_flow
            scheduled[current_flow] += 1
            current_re += 1
        current_flow += 1

    return scheduled, grid
