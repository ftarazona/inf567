class URLLCFlow:
    def __init__(self, period, delay, wcet):
        self.period = period
        self.delay = delay
        self.wcet = wcet

class URLLCJob:
    def __init__(self, flow, deadline, remaining):
        self.flow = flow
        self.deadline = deadline
        self.remaining = remaining

def URLLC_schedule(flows, n_scs, simutime):
    return URLLC_EDF_schedule(flows, n_scs, simutime)

def URLLC_EDF_schedule(flows, n_scs, simutime):
    grid = [[None for i in range(simutime)] for j in range(n_scs)]
    ready_jobs = []
    for step in range(simutime):
        # yield new jobs
        for (i, flow) in enumerate(flows):
            if step % flow.period == 0:
                ready_jobs.append(URLLCJob(i, step+flow.delay, flow.wcet))
        ready_jobs.sort(key=lambda j: j.deadline)
        for scs in range(n_scs):
            if scs < len(ready_jobs):
                grid[scs][step] = ready_jobs[scs].flow
                ready_jobs[scs].remaining -= 1
        ready_jobs = [job for job in ready_jobs if job.remaining > 0]
    return grid
