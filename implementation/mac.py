from embb import *
from urllc import *

import json

def load_flows(file):
    with open(file) as f:
        content = json.loads(f.read())
    embb_flows = [eMBBFlow(f['rate']) for f in content['embb']]
    urllc_flows = [URLLCFlow(f['period'], f['delay'], f['wcet']) for f in content['urllc']]
    return embb_flows, urllc_flows

def schedule(flow_file, n_scs, simutime):
    embb_flows, urllc_flows = load_flows(flow_file)
    scheduled_res, embb_grid = eMBB_schedule(embb_flows, n_scs, simutime)
    urllc_grid = URLLC_schedule(urllc_flows, n_scs, simutime)
    preempted_res = [0] * len(urllc_flows)
    for sc in range(n_scs):
        for step in range(simutime):
            if urllc_grid[sc][step] is not None:
                preempted_res[embb_grid[sc][step]] += 1
                embb_grid[sc][step] = urllc_grid[sc][step] + len(embb_flows)
    for (i, preemptions) in enumerate(preempted_res):
        print(f"eMBB flow {i} preempted {preemptions} times")
        normalized = preemptions / embb_flows[i].rate
        print(f"Normalized: {normalized}")

    return embb_grid

#print(schedule([eMBBFlow(0.4), eMBBFlow(1.0), eMBBFlow(0.1)], [URLLCFlow(5, 5, 2), URLLCFlow(10, 10, 7), URLLCFlow(10, 10, 1)], 2, 50))
grid = schedule('flows1.json', 2, 50)
for subgrid in grid:
    for flow in subgrid:
        print(f"{flow}", end='')
    print()
