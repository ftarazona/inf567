# INF567 Project (2023)

This is a work repository for my INF567 research project on satisfying realtime constraints in 5G systems.

## Bibliography

For starting with the project I will first focus on the 6 documents which can be found in the 'papers' folder. The first three are studies led for 3GPP workshops on recent releases. They aim at surveying or propose enhancement to 5G system implementations for supporting URLLC services. 4 and 5 propose algorithms and optimizations for MAC downlink scheduling. 6 is from task scheduling field and proposes a scheduling algorithm that I think can be of use for MAC realtime scheduling.

Some relevant solutions may be described from the studies. However, for an introduction, it may become very difficult (lots of implementation details probably unknown to students, functional description includes a bunch of non defined acronyms). The idea would be to describe the general idea for the most relevant solutions.

4 is especially relevant for showing how a strategy for scheduling may be relevant for a certain QoS but does not scale for another. Here the strategy is best-effort with respect to delay. That is, scheduling provides no guarantee on a maximum delay. The authors note the issue with URLLC demanding realtime solutions.

5 provides an interesting architecture for MAC scheduling when eMBB and URLLC flows may be dealt with simultaneously. The point is based on the ability of H-ARQ and Error Correction mechanisms to retrieve data even though it was deteriorated, here by puncturing it with URLLC transmissions.

Finally 6 provides a solution for non preemptive realtime scheduling, which could be applied to MAC scheduling by establishing an analogy between task and TB scheduling.

### Refinement of the bibliography

1 and 2 do not particularly deal with issues in MAC layer. Indeed, it remains interesting to have a fine understanding of what is around the MAC layer. Nevertheless, the paper may not be studied deeply for now if it is decided to focus on MAC layer.

3 is fine for haing examples of metrics used when studying mobile network performances. The paper System Level Simulation for 5G URLLC, which I had access then lost it like 10minutes later because damn IEEE, gives other metrics. I will download it on scihub when at Telecom (because SFR blocked the site, damn laws).

7 and 8 were suggested by M. Coupechoux. However they seem to deal more with reliability than latency, which I would rather work on for illustrating scheduling issues. However, reliability may directly impact scheduling, so we keep it, maybe not for a first glance but later.

9 provides good overview of issues raised by new URLLC defined constraints. Together with System Level Simulation (which I still have not downloaded) it also gives a godd definition of these constraints and some typical values required for latency and reliability.

6 effectively comes from another community. My idea with the project is indeed to link network knowledge I will acquire during the course to a field I am already more familiar with. I believe realtime task scheduling may adapt very well to network scheduling when having to deal with hard realtime constraints. 10 to 12 provides background on issues when dealing with non-preemptive scheduling. Preemptive scheduling is much more mature but does not adapt to MAC scheduling since H-ARQ can be interrupted and preempted.

## Proposition of practical work

As a practical work, I would like to illustrate some strategies for implementing multiplexed QoS MAC scheduling.
I think a first step is to do it for downlink. If after analyzing results I have time left, I might get into uplink.

### Refinement of my proposition

After what has been added to my bibliography, I believe it is important to motivate my choice of bringing work from another community. Indeed I must be primarily concerned about defining about what may be approved statistically, i.e. by measuring and reaching a certain proportion of good behaviour, and what must be enforced "whatever it takes".
In realtime systems, this is analog to the difference between hard realtime, soft realtime, and best effort.

In order not to fall into a pure task scheduling problematic, I must ensure that I include network issues. It will therefore be necessary that I clearly demonstrate how network specific constraints and parameters may impact scheduling and how it can be translated into task scheduling concepts.
