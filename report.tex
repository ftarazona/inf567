\documentclass[12pt]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[sorting=none]{biblatex}
\addbibresource{references.bib}

\title{Realtime constraints in 5G systems}
\author{Florian Tarazona, Marceau Coupechoux, Juan-Antonio Cordero-Fuertes}
\date{}

\begin{document}

\maketitle

This document is my report for INF567 course project. I decided to present why and how the 5G NR standard provides support for realtime applications. As I have some knowledge of such applications, I will also emphasize on implementation issues which may be arised.

\newpage

\section{Introduction}

Realtime applications can be defined as applications which must satisfy time constraints in addition of obviou functional constraints. Take for example a washer. The controller of such a system has to provide a correct temperature control, this is a functional constraint. However it also needs to send a stop signal within a certain deadline: we do not want our clothes to be washed for three hours! Indeed, in this example, the scale of the time constraint is fairly big, say a few minutes. In more critical reatime systems, the deadlines might go down to only a few milliseconds.

Up to now, such systems used to be still: components of a plant would remain at their place, and if it was necessary to rearrange them, adequate reconfiguration had to be led on the controller, which most of the time involves human action. However, for optimization issues, it would be convenient to be able to provide mobility to such systems. In industry, moving production units easily may allow for greater speed of production and more adaptability to market demand. Some applications are even dependent on such mobility by nature, take for instance autonomous vehicles, remote control. Paragraph 7.2.2 in \cite{TS22261} gives more examples of realtime applications that would need mobility, and \cite{TS22104,TS22186,TS22289}\\

The 5G NR standard defines several Qualities of Service meant to allow such applications to rely on it. Such are defined in paragraph 6.1.7 of \cite{TS22203}. Among such, the QoS 85 is particularly interesting as the required delay (5 ms) could not be met with 4G as a frame lasted 10 ms. It also requires a very high reliability (0.99999\% success). Nevertheless the packet size is usually smaller than when higher latency is required. Note that there is always a balance between \textit{delay} and \textit{reliability}: a greater reliability should require more delay to be allowed in order to be achieved. We will come back to it later.

\newpage
\section{Problem statement and challenges}

The survey in \cite{TR38824} presents two interesting metrics for measuring how well a 5G networks manages to meet requirements for URLLC users \textit{(see 5.1)}. A first option is to measure the percentage of users for which the network meets the requirements. This option is relevant for fixed systems, \textit{i.e.} when both the UEs and their traffic are known. This may be the case in systems such as factories, where the UEs would be different unit productions or machines sending sensor data and receiving commands. A second option is to measure the URLLC \emph{capacity} which takes into account the eMBB traffic intended to be multiplexed with URLLC traffic. This may be wanted when URLLC traffic is added to preexistent networks. Let's take V2X communication as an example. It could be possible to install new base stations in addition of preexisting ones which serve eMBB users (voice or data services), but this may lead to oversizing. Moreover, there would be a risk of interference anyway, so that the eMBB traffic must be taken into account when measuring the performance for URLLC traffic in such situations.

Still in \cite*{TR38824} \textit{Tables 5.2.1-1, 5.2.2-1, 5.2.3-1, 5.2.3-1, 5.2.4-1}, the results of the survey show that, for a small number of UEs, requirements for downlink communication are usually met for more than 90\% of the UEs. However, the result is generally lower for uplink communication, though a grant free strategy shows good results in \textit{Table 5.2.4-1}. One may notice a low resource utilization quite decorrelated from the percentage of well served UEs. An interpretation of it may be that giving more resource to a 5G system for meeting URLLC requirements is irrelevant.\\

Though results seem fairly good overall for a small number of UEs, they present two issues: they do not scale well for a larger number of users and they remain undeterministic, \textit{i.e. a realtime system may be easily and randomly put in a state of failure due to a missed requirement}. When designing such realtime applications, what is generally required is guarantees. For instance, the application may be able to handle a 5\% probability of failure in UEs communications (a failure in this case is either a reliability less than 99.999\% or say a delay greater than 3ms). The 5G system is therefore expected to provide a guarantee that for a certain number of UEs, it will be able to ensure communications with such a probability, while certain assumptions hold, particularly on the state of the channel.\\

As already said, URLLC services have been added to the 5G standard so that realtime application may rely on mobile configurations. The range of application fields is wide: factory automation, autonomous vehicles, medical remote surgery, augmented reality. Some of these fields are angular in the global economy and their perspectives if they can rely trustably on 5G realtime support are huge. Others are still fields of research and are believed to become major technologies in the future such as autonomous vehicles or medical remote surgery. There is an overall major challenge for being able to build critical realtime applications on top of 5G networks.

\newpage
\section{MAC scheduling for realtime support}

In this section we will describe why the MAC layer plays a major role in enforcing realtime constraints at a gNB. We will also discuss scheduling issues and possible solutions that address them. For simplicity sake, we first consider downlink traffic only. At the end of the section we will propose a discussion about uplink traffic.\\

At the base station, when a message is to be transmitted to a UE, it goes down from the SDAP layer to the PHY layer. At the SDAP layer it is assigned to a radio bearer. A radio bearer is a set of parameters which will be used to describe the packet constraints, the manner in which it will be transmitted, for instance the channel code to be used, the expected probability of failure. PDCP layer encrypts and compress the packet, then transmits it RLC layer. At the RLC layer, three modes are possible and are closely related to realtime or non-realtime traffic.

The RLC layer can transmit a packet using three different modes: Transparent Mode, Unacknowledged Mode and Acknowledged Mode. In the latter mode, the whole packet may be retransmitted by an ARQ mechanism in order to ensure a certain reliability in case the MAC layer would fail to transmit some parts of it. In Transparent and Unacknowledged modes, the packet is processed then given to the MAC layer and there is no ARQ mechanism. Packets considered as realtime generally use such unacknowledged modes. While it may seem counter-intuitive as it may result in a poorer reliability, there is actually a trade-off with time constraints: a retransmission at the RLC is much more expensive than at the MAC layer: instead of retransmitting a part of the packet, we retransmit the whole packet and go through multiple MAC transmissions over again. This may take several milliseconds which can not be afforded for certain applications.

Therefore the MAC layer has to ensure both high reliability and time constraints for realtime packets. In order to ensure reliability, it can retransmit part of the SDU using a H-ARQ mechanism. Delay enforcement is ensured by the scheduler. Note that reliability and delay are closely related as higher the reliability, higher the number of transmission attempts, higher the delay. The MAC scheduler must take this into account when allocating transport blocks to the input traffic.\\

We may also note that the challenge does not only reside in scheduling realtime packets only, but rather multiplex them with other packets, say eMBB. Indeed, it would be possible to reserve certain bandwidth parts for realtime traffic. But in practice, as realtime traffic is usually sparse and consists in small packets, doing so results in oversizing. As a consequence, realtime traffic should share bandwidth parts with non-realtime traffic.\\

Nevertheless, there remains a gap between realtime guarantees provided by most 5G implementations and realtime guarantees expected for critical systems. Up to now, mobile networks had to focus more on data rate optimization and secondarily fairness among users. When it comes to scheduling, this leads to a wide use of the well-known Proportional Fair scheduling policy. However, PFair quite fails to provide guarantee for realtime traffic. \cite*{mandelli2019} adapts PFair to support multiple slices. Nevertheless the authors consider eMBB slices and acknowledge that the extension of their version of PFair to URLLC slices remains an open question (cf. IV.C.). Using a PFair scheduling for both eMBB and URLLC constraints may eventually lead to support for soft realtime traffic (\textit{i.e. allowed to miss constraints under certain conditions}), but hard realtime constraints support still arise major difficulties.

Yin \textit{et al} propose an interesting approach for realtime support implementation multiplexing eMBB traffic in \cite*{yin2021}. Their first idea is to consider URLLC and eMBB with distinct scheduling policies. URLLC traffic might therefore use a dedicated realtime scheduler, while the eMBB scheduler focuses on data rate and fairness. The second idea is to use puncturing to puncture URLLC traffic over eMBB traffic. This allows two important properties:
\begin{itemize}
    \item URLLC traffic will be prioritized over eMBB traffic.
    \item eMBB traffic may still be corrected by MAC error detection and correction mechanisms.
\end{itemize}

This work also adopts an adequate approach for realtime scheduling. It starts with a calculation of the worst case time it will take to send a packet, so that the scheduling can ensure the enforcement of the deadline. The worst case time takes into account the expected reliability and the channel conditions. The scheduling problem is then formulated as an Integer Problem to minimize the impact on eMBB traffic.

To be more precise, their work proposes to model the URLLC scheduler decisions for each user by a vector of length the number of minislots. This vector contains 0s and 1s depending on the decision for each resource whether the user takes it. It then expresses the data rate for each eMBB user as a function of these vectors. Then the problem is to find such vectors which maximizes the sum of a utility function applied to each data rate (here this function is \(log\)). Three solutions are studied for maximizing this sum: brute-forcing, which results in an NP-hard problem; convex relaxation which gives a near-optimal result; and a greedy algorithm which is much faster to run at the cost of a sub-optimal solution.

Finally the metric used for evaluating the algorithm relies on how well the eMBB traffic is conserved via the utility. This corresponds to the second metric described in \cite{TR38824} and proves the algorithm to scale to adaptable systems. However the number of URLLC users remains low (about 10 users). When increased above 30, the utility falls down to 0.\\

The paper treats downlink traffic. However in several use cases for realtime applications uplink traffic must be handled (say for sensors). A first approach is to use TDD and reserve certain time resources exclusively for uplink transmissions. Several issues however with this approach: first this may lead to oversizing the system. Then if a packet with a very tight deadline were to arrive at the beginning of an uplink period, it might be impossible for the scheduler to meet its requirements. Finally, even in the normal case the scheduler must adapt to this new constraint. Another approach could be FDD, that is reserve some bandwidth to uplink. This may still lead to oversizing but makes adaptation much easier. Yet another approach is grant-free access which \cite*{TR38824} presents to yield good results. In a realtime system nevertheless, note this may introduce non-determinism: an uplink packet may collide with a realtime downlink constraint, leading to an overrun. This is a trade-off between determinism and efficiency. If there is a lot of uplink traffic, reserving resources may not be such a big oversizing and would make the system more deterministic. On the contrary, with a sparse uplink traffic, we may deem the collision probability with a downlink realtime packet unsignificant compared to the oversizing that may result from reserving resources.\\

In this project I propose a simulation of how the architecture described by Yin \textit{et al} may process multiplexes URLLC and eMBB traffics. 

\newpage
\section{Implementation}

The purpose of the implementation I propose is to illustrate the multiplexing proposed in \cite*{yin2021}. In particular the idea of puncturing and measuring the impact on the eMBB traffic.\\

The grid of resource elements is described by the number of subcarriers. We assume only one numerology.

The schedulers for eMBB and URLLC are implemented in \textit{embb.py} and \textit{urllc.py} respectively. The MAC layer reads the flows from a JSON file, then gets a grid for eMBB packets from the eMBB scheduler and a grid for URLLC packets from the URLLC scheduler. It finally punctures the former with the latter and gives some statistics about how URLLC scheduling impacted eMBB scheduling.

I gave the example of an Earliest Deadline First scheduler, which presents several differences with the algorithm proposed in the paper. Such a policy is much simpler to implement and does not need to solve an optimization problem. The basic EDF scheduler is unaware of the eMBB scheduling, so that it does not try to optimize the utility for eMBB traffic. In the paper such an improvement is solved by using a non work-conserving policy. That is, although some packets may be transmitted for resource elements are still available, the scheduler decides to wait for the next slot in order not to puncture eMBB traffic. The trade-off is that, if the URLLC traffic is unknown, at the next slot there may arrive an overload, resulting in some packets being unable to meet their latency requirements.\\

The implementation should be improved by adding netowrk related parameters for achieving realitistic simulations, for instance the BLER, numerologies, real deadlines given in the standard.

\newpage
\printbibliography

\end{document}